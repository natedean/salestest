import React from 'react';
import ReduxExample from '../components/reduxExample/ReduxExample';
import {connect} from 'react-redux';
import showNotification from '../actions/showNotification';

const mapStateToProps = state => ({
	externalExamplesLink: state.externalExamplesLink,
	notificationCount: state.notificationCount
});

const mapDispatchToProps = {
	showNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(ReduxExample);
