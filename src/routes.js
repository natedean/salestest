import React from 'react';

import {createMenu} from 'shared/services/menuHelpers';

import LazySales from './components/lazy/LazySales';
import LazyCCA from './components/lazy/LazyCCA';
import LazySelectCare from './components/lazy/LazySelectCare';
import LazyAdmin from './components/lazy/LazyAdmin';

const links = [
	{path:'/sales', name:'Sales', subRoutes: [
		{path:'/cca', name:'CCA'},
		{path:'/selectcare', name:'SelectCare'},
		{path:'/admin', name:'Admin'}
	]}
];

export const createRoutes = (store) => {
	const salesRoutes = createMenu(links)[0].subRoutes;

	return [
		{path:'/sales', name:'Sales', component: () => <LazySales store={store} routes={salesRoutes} />, subRoutes: [
			{path:'/cca', name:'CCA', component: () => <LazyCCA store={store} />},
			{path:'/selectcare', name:'SelectCare', component: () => <LazySelectCare store={store} />},
			{path:'/admin', name:'Admin', component: () => <LazyAdmin store={store}/>}
		]}
	];
};
