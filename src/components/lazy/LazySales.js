import React from 'react';
import LazyComponent from './LazyComponent';

const LazySales = (props) => (
	<LazyComponent
		{...props}
		getComponent={(callback) => {
			require.ensure([], require => {
				callback(null, require('../home/Home'));
			}, 'Sales'); // Domain-Subdomain
		}}
	/>
);

export default LazySales;
