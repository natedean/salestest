import React from 'react';
import LazyComponent from './LazyComponent';

const LazyAdmin = (props) => (
	<LazyComponent
		{...props}
		getComponent={(callback) => {
			require.ensure([], require => {
				callback(null, require('../Admin'));
			}, 'Sales-Admin'); // Domain-Subdomain
		}}
	/>
);

export default LazyAdmin;
