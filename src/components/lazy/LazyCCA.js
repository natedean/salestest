import React, {Component} from 'react';
import LazyComponent from './LazyComponent';
import {injectAsyncReducer, removeAsyncReducer} from 'shared/configureStore';

class LazyCCA extends Component {

	componentWillUnmount() {
		removeAsyncReducer(this.props.store, 'CCA');
	}

	render () {
		return (
			<LazyComponent
				{...this.props}
				getComponent={(callback) => {
					require.ensure([
						'../CCA',
						'../../reducers/CCA'
					], require => {
						injectAsyncReducer(this.props.store, 'CCA', require('../../reducers/CCA').default);
						callback(null, require('../CCA'));
					}, 'Sales-CCA'); // Domain-Subdomain
				}}
			/>
		);
	}
}


export default LazyCCA;
