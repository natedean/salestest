import React, { PropTypes } from 'react';

class LazyComponent extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			AsyncModule: null
		};
	}

	componentDidMount() {
		this.props.getComponent((_, module) => {
			this.setState({
				AsyncModule: module.default || module
			});
		});
	}

	render() {

		const { AsyncModule } = this.state;

		if (AsyncModule) {
			return (<AsyncModule {...this.props} />);
		}

		return (<div>Loading...</div>);
	}

}

export default LazyComponent;

LazyComponent.propTypes = {
	getComponent: PropTypes.func.isRequired
};
