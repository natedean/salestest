import React, {Component} from 'react';
import LazyComponent from './LazyComponent';
import {injectAsyncReducer, removeAsyncReducer} from 'shared/configureStore';

// this name (require.ensure arg below) must be unique for chunking to work correctly! Convention is Domain-Subdomain
class LazySelectCare extends Component {

	componentWillUnmount() {
		removeAsyncReducer(this.props.store, 'SelectCare');
	}

	render () {
		return (
			<LazyComponent
				{...this.props}
				getComponent={(callback) => {
					require.ensure([
						'../SelectCare',
						'../../reducers/SelectCare'
					], require => {
						injectAsyncReducer(this.props.store, 'SelectCare', require('../../reducers/SelectCare').default);
						callback(null, require('../SelectCare'));
					}, 'Sales-SelectCare'); // Domain-Subdomain
				}}
			/>
		);
	}
}

export default LazySelectCare;
