import {combineReducers} from 'redux';

import isNotifying from './isNotifying';
import notificationMessage from './notificationMessage';
import externalExamplesLink from './externalExamplesLink';
import notificationCount from './notificationCount';

const rootReducer = combineReducers({
	isNotifying,
	notificationMessage,
	externalExamplesLink,
	notificationCount
});

export default rootReducer;
